// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require materialize-sprockets
//= requie  urlive
//= require highcharts
//= require_tree .

$(document).ready(function() {
  if ($('#infinite-scrolling').size() > 0) {
   $(window).on('scroll', function() {
     var more_posts_url;
     more_posts_url = $('.pagination a[rel=next]').attr('href');
     if (more_posts_url && $(window).scrollTop() > $(document).height() - $(window).height() - 60) {
       $('.pagination').html("");
       $.ajax({
         url: more_posts_url,
         success: function(data) {
           return $("#posts").append(data);
         }
       });
     }
     if (!more_posts_url) {
       return $('.pagination').html("");
     }
   });
 }
});

$(document).ready(function() {
  $(".button-collapse").sideNav();
  $.ajaxSetup({
    statusCode: {
    401: function(){
    location.href = "/users/sign_in";
    }
   }
  });
  // twitter script
  !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
});
