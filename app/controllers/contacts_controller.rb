class ContactsController < ApplicationController

	def new
    @contact = Contact.new
  end

  def create
    #raise params[:contact].to_yaml
    @contact = Contact.new(contacts_params)
    @contact.request = request
    if @contact.deliver
      redirect_to '/', notice: 'Thank you for your message. We will contact you soon!'
    else
      redirect_to '/', error: 'Cannot send message.'
    end
  end

  def contacts_params
    params.require(:contact).permit(:name, :email, :message, :nickname)
  end

end
