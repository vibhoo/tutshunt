class LinkController < ApplicationController
  
  def check
    if params[:name].present?
      check = Post.find_by_name(params[:name])
      first_or_update check.id
      redirect_to "#{check.link}"
    else
      redirect_to '/', notice: 'You are not authorize to perfrom this action'
    end
  end

  # def image_click
  # 	check = Post.find_by_link(params[:value])
  # 	first_or_update check.id
  # end

   def first_or_update id
  	Analytic.where(post_id: id).first_or_create.increment!(:visit)
  end

end
