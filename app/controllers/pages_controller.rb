class PagesController < ApplicationController
  impressionist :actions =>[:index]
  def index
  	name  = []
    visit = []
    link  = []
    name_n_visit = Analytic.select('posts.name,analytics.visit, posts.link').joins(:post).order(visit: :desc).limit(8)
    name_n_visit.each do |item|
      name << item.name
      visit << item.visit
      link << item.link
    end
    @name  = name
    @visit = visit
    @link  = link
  end

  def about
  end

  def privacy
  end

end
