class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy,:vote]
  before_filter :authenticate_user!, except: [:sign_out_redirect, :index,:show,:search]
  respond_to :html

  def index
    @posts = Post.where(on_hold: '0').paginate(:page => params[:page], :per_page => 10).order(created_at: :desc)
    respond_with(@posts)
  end

  def show
    @user  = @post.votes_for.up.by_type(User).voters
    impressionist(@post,"#{@post.name}")
    respond_with(@post)
  end

  def new
    @post = Post.new
    respond_with(@post)
  end

  def edit
  end

  def create
    @post = current_user.posts.new(post_params)
    @post.save
    respond_with(@post)
  end

  def update
    @post.update(post_params)
    respond_with(@post)
  end

  def destroy
    if current_user == @post.user
      @post.destroy
      redirect_to @post, notice: 'Tutorial Have deleted'
    else
      redirect_to @post, notice: "You don't have permission."
    end
  end

  def vote
    current_user.likes @post
    respond_to do |format|
      #format.json {render json:{count: @post.liked_count }}
      format.js { render :vote, locals: {error_message: "Something went wrong",count: @post.votes_for.size}}
    end
  end

  def search
    @result = Post.search(params[:q])
  end

  def users_posts
    @posts = current_user.posts
  end

  def likes
    @posts = current_user.get_voted Post
  end

  private
  def set_post
    begin
      @post = Post.friendly.find(params[:id])
      redirect_to action: action_name, id: @post.friendly_id, status: 301 unless @post.friendly_id == params[:id]
    rescue ActiveRecord::RecordNotFound
      redirect_to :controller => "posts", :action => "index"
      return
    end
  end

  def post_params
    params.require(:post).permit(:name, :link, :tags, :category)
  end
end
