module ApplicationHelper
	def avatar_url user, size
		if user.image.present?
			image_tag(user.image, alt: user.name, class: "image-round", "data-content" => user.name)
		else
	   	#default_url = Rails.root.to_s + 'public/images/guest.png/'
	  	gravatar_id  = Digest::MD5.hexdigest(user.email.downcase)
	  	gravatar_url = "http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}&d=retro"
	  	image_tag(gravatar_url, alt: user.email, class: "image-round","data-content" => user.email)
	  end
	end

	def avatar_index_url user, size
		if user.image.present?
			image_tag(user.image, alt: user.name, class: "image-round", "data-content" => user.name, "data-variation" => "inverted", size: "30x30")
		else
	   	#default_url = Rails.root.to_s + 'public/images/guest.png/'
	  	gravatar_id  = Digest::MD5.hexdigest(user.email.downcase)
	  	gravatar_url = "http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}&d=retro"
	  	image_tag(gravatar_url, alt: user.email, class: "image-round","data-content" => user.email,"data-variation" => "inverted")
	  end
	end
end
