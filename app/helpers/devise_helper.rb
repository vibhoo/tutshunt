# /app/helpers/devise_helper.rb

module DeviseHelper
  def devise_error_messages!
    return '' if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t('errors.messages.not_saved',
      count: resource.errors.count,
      resource: resource.class.model_name.human.downcase)

    html = <<-HTML
    <script type="text/javascript">
      var toastContent = "<span>#{messages}</span>";
      Materialize.toast(toastContent, 10000, 'rounded increase-height')
    </script>
    HTML
    html.html_safe
  end
end
