ThinkingSphinx::Index.define :post, :with => :real_time do
  indexes name
  set_property :enable_star => true
  set_property :match_mode => :any
end