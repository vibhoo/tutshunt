class UserMailer < ActionMailer::Base
  default from: "\"Auffer\"  <info@auffer.in>"
  layout false
  def auffer_mail mail
    mail(to: mail, subject: "Auffer.in - Instant deals on the go!").deliver
  end

  def exchange
    emails = ExternalEmail.all.offset(5915).limit(600)
    emails.each do |variable|
      mail(to: variable.email, subject: "Hello #{variable.name}, we would love to know your thoughts on spare tickets and coupons?", from: "info@exchange.com").deliver
    end
  end
end
