class Post < ActiveRecord::Base
	 acts_as_votable
	 extend FriendlyId
	 is_impressionable
	 has_many :analytics
	 belongs_to :user
   friendly_id :name, use: :slugged
	# validates_presence_of :name
	#after_save ThinkingSphinx::RealTime.callback_for(:post)

	def self.recommend
		post_id = rand(1..Post.count)
		if Post.exists?(post_id)
			recommend = Post.find(post_id)
		else
			recommend = Post.find(1)
		end
	end
end
