class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,:omniauthable,:confirmable
  acts_as_voter
 # has_many :authentications
  has_many :posts

 def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    if user
      return user
    else
      registered_user = User.where(:email => auth.info.email).first
      if registered_user
        return registered_user
      else
        user = User.new(name:auth.extra.raw_info.name,provider:auth.provider,uid:auth.uid,email:auth.info.email,password:Devise.friendly_token[0,20],image:auth.info.image)
        user.skip_confirmation!
        user.confirm!
        user.save
        user
      end
    end
  end


  def self.find_for_twitter_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    if user
      return user
    else
      registered_user = User.where(:email => auth.uid + "@twitter.com").first
      if registered_user
        return registered_user
      else
        user = User.new(name:auth.extra.raw_info.name,provider:auth.provider,uid:auth.uid, email:auth.uid+"@twitter.com",  password:Devise.friendly_token[0,20],image:auth.info.image)
        user.skip_confirmation!
        user.confirm!
        user.save
        user
      end
    end
  end
  
end
