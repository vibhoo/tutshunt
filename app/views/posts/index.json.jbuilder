json.array!(@posts) do |post|
  json.extract! post, :id, :name, :link, :tags, :category
  json.url post_url(post, format: :json)
end
