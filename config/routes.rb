Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  if Rails.env.development?
   mount LetterOpenerWeb::Engine, at: "/letter_opener"
  end

  resources :posts,:path => '/tutorial', except:[:delete] do
  	member do
      put "like", to: "posts#vote"
    end
  end

  devise_for :users, :controllers => { omniauth_callbacks: 'omniauth_callbacks' }
  resources :users

  get 'link/check'
  get  'pages/about'
  get  'pages/privacy'
  get  '/trending' => 'pages#index'
  root 'posts#index'

  get '/contact' => 'contacts#new'
  resources "contacts", only: [:new, :create]
  get '/search' => 'posts#search', as: 'search'
  get "/users_posts" => 'posts#users_posts'
  get '/likes' => "posts#likes"
  match "*path" => redirect("/"),  via:[:get, :post] #redirect user to home if wrong url entered
end
