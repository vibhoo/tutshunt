class CreateAnalytics < ActiveRecord::Migration
  def change
    create_table :analytics do |t|
      t.integer :post_id
      t.integer :visit , default: 0

      t.timestamps
    end
  end
end
