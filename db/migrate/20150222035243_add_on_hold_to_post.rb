class AddOnHoldToPost < ActiveRecord::Migration
  def change
    add_column :posts, :on_hold, :boolean, default: false
  end
end
