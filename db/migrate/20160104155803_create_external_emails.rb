class CreateExternalEmails < ActiveRecord::Migration
  def change
    create_table :external_emails do |t|
      t.string :email

      t.timestamps
    end
  end
end
