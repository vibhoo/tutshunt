namespace :db do
  desc "Increase vote"
  task increase_vote: :environment do
     increase_view
  end
end

def self.increase_view
	post = Post.all
	post.each do |item|
   impressionable_type = "Post"
   impressionable_id = item.id
   controller_name = "posts"
   action_name = "show"
   view_name = "NULL"
   request_hash = Digest::SHA256.hexdigest(item.name)
   ip_address = Faker::Internet.ip_v4_address
   session_hash = Digest::MD5.hexdigest(item.link)
   referrer = "http://localhost:3000/"
   Impression.create!(impressionable_type: impressionable_type,
     impressionable_id: impressionable_id,
     controller_name: controller_name,
     action_name:action_name,
     view_name:view_name,
     request_hash:request_hash,
     ip_address: ip_address,
     session_hash:session_hash,referrer:referrer
    )
  end
  puts "COMPLETE!\n\n"
end
