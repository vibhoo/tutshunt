namespace :db do
  desc "Populate database with faker data"
  task populate: :environment do
    # make_users
    # make_posts
    make_votes
  end
end

# def make_users
#   puts "\n\nCREATING 10 USERS..."

#   10.times do
#     name = "#{Faker::Name.first_name[0..5]} #{Faker::Name.last_name[0..6]}".gsub(/[^\w+\s]/, "")
#     email = Faker::Internet.email
#     password = "secret123"
    
#     User.create!(email: email,
#       name: name,
#       password: password,
#       password_confirmation: password,
#     )
#   end

#   puts "COMPLETE!\n\n"
# end

# def make_posts
#   puts "CREATING 10 Posts ENTRIES..."

#   30.times do
#     user = User.offset(rand(User.count)).first
#     name = Faker::Lorem.sentence
#     link = Faker::Internet.url
#     begin
#       user.posts.create!(
#         name: name,
#         link: link
#       )
#     rescue ActiveRecord::RecordNotUnique
#     end
#   end

#   puts "COMPLETE!\n\n"
# end

def make_votes
  puts "CREATING 100 Votes"

  100.times do
    user = User.offset(rand(User.count)).first
    post = Post.offset(rand(Post.count)).first

    user.likes(post) unless user == post.user 
  end

  puts "COMPLETE!\n\n"
end